console.log("hello world");

let cities = [ // array of JS object
  
  {city: "Manhattan",state: "New York",country: "USA"},
  {city: "New York",state: "New York",country: "USA"},
  {city: "Jersey",state: "New Jersey",country: "USA"},

];
console.log(cities);

let cities2 = [ // example of JSON
  
  {"city": "Manhattan","state": "New York","country": "USA"},
  {"city": "Manhattan","state": "New York","country": "USA"},
  {"city": "Manhattan","state": "New York","country": "USA"},

];
console.log(cities);

// JSON is used to serialize diff data types into bytes

let batchArray = [
  {batchName: "Batch X"}, {batchName: "Batch Y"}
];
console.log(batchArray);
console.log("result from stringify method");
console.log(JSON.stringify(batchArray)); // convert JS objects to strings

let data = [
  {dataName: "string",
age: 55,
address: {
  city: "London",
  country: "UK"
}}
]

console.log(JSON.stringify(data));



;